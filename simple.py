from collections import deque
from threading import Thread
from common import Website, dedupe_unhashable


if __name__ == '__main__':

    website_collection = deque([], 6)

    website = Website(name='Google FR', url='https://www.google.fr')
    website_collection.append(website)

    website = Website(name='Google COM', url='https://www.google.com')
    website_collection.append(website)

    website = Website(name='Google CAT', url='https://www.google.cat')
    website_collection.append(website)

    website = Website(name='Google CAT', url='https://www.google.cat')
    website_collection.append(website)

    website = Website(name='Google IT', url='https://www.google.it')
    website_collection.append(website)

    website = Website(name='Google PT', url='https://www.google.pt')
    website_collection.append(website)

    for website in website_collection:
        website.fetch_url()
        print("{} updated at {} with status_code {}".format(website, website.updated, website.status_code))

    # sort by updated, from most resent to oldest
    website_collection_sorted = sorted(website_collection, key=lambda w: w.updated, reverse=True)
    print(website_collection_sorted)

    # remove duplicates
    website_collection_unique = list(
        dedupe_unhashable(
            website_collection_sorted,
            key=lambda w: (w.name, w.url)
        )
    )
    print(website_collection_unique)
    print("Length of unique collection list: {}".format(len(website_collection_unique)))

    # compare objects
    google_fr = Website(name='Google FR', url='https://www.google.fr')
    google_com = Website(name='Google COM', url='https://www.google.com')
    google_fr2 = Website(name='Google FR', url='https://www.google.fr')

    print("Google FR is Google COM? {}".format(google_fr is google_com))
    print("Google FR is Google FR2? {}".format(google_fr is google_fr2))
    print("Google FR == Google FR2? {}".format(google_fr == google_fr2))

    # remove duplicates using a set
    website_collection_set = set(website_collection)
    print("Length of unique collection set: {}".format(len(website_collection_set)))
