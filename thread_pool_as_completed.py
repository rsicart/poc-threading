from concurrent import futures
from common import Website, fetch_url


if __name__ == '__main__':

    #
    # Multithread with ThreadPoolExecutor and result list
    #
    website_collection = []

    website = Website(name='Google FR', url='https://www.google.fr')
    website_collection.append(website)

    website = Website(name='Google COM', url='https://www.google.com')
    website_collection.append(website)

    website = Website(name='Google CAT', url='https://www.google.cat')
    website_collection.append(website)

    website = Website(name='Google CAT', url='https://www.google.cat')
    website_collection.append(website)

    website = Website(name='Google IT', url='https://www.google.it')
    website_collection.append(website)

    website = Website(name='Google PT', url='https://www.google.pt')
    website_collection.append(website)

    with futures.ThreadPoolExecutor(max_workers=2) as executor:
        to_do = []
        for website in website_collection:
            future = executor.submit(fetch_url, website)
            to_do.append(future)
            print('Scheduled for {}: {}'.format(website, future))

        results = []
        for future in futures.as_completed(to_do):
            res = future.result()
            print('{} result: {}'.format(future, res))
            results.append(res)

    print("results size: {}".format(len(results)))
