from time import sleep
import urllib.request
from datetime import datetime


class Website:

    def __init__(self, name=None, url=None):
        self.name = name
        self.url = url
        self.status_code = None
        self.updated = None

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, data):
        self._name = data

    @property
    def url(self):
        return self._url

    @url.setter
    def url(self, data):
        self._url = data

    @property
    def status_code(self):
        return self._status_code

    @status_code.setter
    def status_code(self, data):
        self._status_code = data

    @property
    def updated(self):
        return self._updated

    @updated.setter
    def updated(self, data):
        self._updated = data

    def __repr__(self):
        return "{}(name='{}', url='{}')".format(self.__class__.__name__, self.name, self.url)

    def __eq__(self, other):
        return (self.name == other.name and self.url == other.url)

    def __hash__(self):
        return hash((self.name, self.url))

    def fetch_url(self):
        try:
           response = urllib.request.urlopen(self.url)
           self.status_code = response.code
        except HTTPError as e:
           self.status_code = e.code
        self.updated = datetime.now()
        print("{} updated at {} with status_code {}".format(self, self.updated, self.status_code))

def dedupe_hashable(items):
    """Remove duplicates from a sequence of hashable types
    """
    seen = set()
    for item in items:
        if item not in seen:
            yield item
            seen.add(item)

def dedupe_unhashable(items, key=None):
    """Remove duplicates from a sequence of unhashable types
    """
    seen = set()
    for item in items:
        val = item if key is None else key(item)
        if val not in seen:
            yield item
            seen.add(val)

def consumer_threads(in_q, _sentinel):
    while True:
        data=in_q.get()
        if data is _sentinel:
            in_q.put(_sentinel)
            break
        data.fetch_url()
        sleep(1)

def consumer(in_q, _sentinel):
    while True:
        data=in_q.get()
        if data is _sentinel:
            in_q.put(_sentinel)
            break
        data.fetch_url()
        print(in_q.qsize())
        sleep(1)

def fetch_url(website):
    website.fetch_url()
    sleep(1)
    return website

