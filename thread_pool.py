from queue import Queue
from concurrent import futures
from common import Website, consumer


if __name__ == '__main__':

    #
    # multithread with ThreadPoolExecutor
    #

    qq = Queue()

    website = Website(name='Google FR', url='https://www.google.fr')
    qq.put(website)

    website = Website(name='Google COM', url='https://www.google.com')
    qq.put(website)

    website = Website(name='Google CAT', url='https://www.google.cat')
    qq.put(website)

    website = Website(name='Google CAT', url='https://www.google.cat')
    qq.put(website)

    website = Website(name='Google IT', url='https://www.google.it')
    qq.put(website)

    website = Website(name='Google PT', url='https://www.google.pt')
    qq.put(website)

    # object that signals shutdown
    _q_sentinel = object()
    qq.put(_q_sentinel)

    pool = futures.ThreadPoolExecutor(2)
    worker_1 = pool.submit(consumer, qq, _q_sentinel)
    worker_2 = pool.submit(consumer, qq, _q_sentinel)

    result_1 = worker_1.result()
    result_2 = worker_2.result()

    print(result_1)
    print(result_2)
