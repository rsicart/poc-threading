from queue import Queue
from threading import Thread
from common import Website, consumer_threads


if __name__ == '__main__':

    #
    # multithread
    #

    website_collection = []
    q = Queue()

    website = Website(name='Google FR', url='https://www.google.fr')
    website_collection.append(website)
    q.put(website)

    website = Website(name='Google COM', url='https://www.google.com')
    website_collection.append(website)
    q.put(website)

    website = Website(name='Google CAT', url='https://www.google.cat')
    website_collection.append(website)
    q.put(website)

    website = Website(name='Google CAT', url='https://www.google.cat')
    website_collection.append(website)
    q.put(website)

    website = Website(name='Google IT', url='https://www.google.it')
    website_collection.append(website)
    q.put(website)

    website = Website(name='Google PT', url='https://www.google.pt')
    website_collection.append(website)
    q.put(website)

    # object that signals shutdown
    _sentinel = object()
    q.put(_sentinel)


    print("Length of website collection list before: {}".format(len(website_collection)))

    consumer_1 = Thread(target=consumer_threads, args=(q, _sentinel,))
    consumer_2 = Thread(target=consumer_threads, args=(q, _sentinel,))
    consumer_1.start()
    consumer_2.start()
    consumer_1.join(timeout=30)
    consumer_2.join(timeout=30)

    print("Length of website collection list after: {}".format(len(website_collection)))

    ## sort by updated, from most resent to oldest
    #website_collection_sorted = sorted(website_collection, key=lambda w: w.updated, reverse=True)
    #print("Length of website collection list: {}".format(len(website_collection_sorted)))
    #print(website_collection_sorted)

    for website in website_collection:
        print("{}: updated at {}".format(website, website.updated))
